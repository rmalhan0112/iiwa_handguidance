import pandas as pd
from pandas import DataFrame
import numpy as np
import cmath as mth

joint_angles = np.array(pd.read_csv("Joint_Readings.csv"))
waypoints = []

print("Original Size:  ",joint_angles.shape[0])

for i in range(0,joint_angles.shape[0]):

    flag = False
    j = i

    while ( flag==False and j!=(joint_angles.shape[0]-1)):
        state_change = joint_angles[j + 1][:] - joint_angles[i][:]
        if (np.linalg.norm(state_change, np.inf) > (5 * mth.pi / 180) ):
            waypoints.append(joint_angles[j])
            flag = True
        else:
            j += 1

    if (j+1==joint_angles.shape[0]):
        waypoints.append(joint_angles[j])
        break
    else:
        i = j+1

waypoints = np.array(waypoints).reshape(len(waypoints),7)
print("Final Size:  ",waypoints.shape[0])
df = DataFrame(waypoints)
df.to_csv("Waypoints.csv",index=False,header=False)